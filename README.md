
P9 Jonathan de la osa: MEDISCREEN <br/>
 <br/>
## *Architecture *
Patterns-microservices-springcloud avec <br/>
Spring boot maven <br/>
MySql -PhpMyAdmin <br/>
MongoDB - mongodbCompass <br/>
GitLab: https://gitlab.com/jonathan_fullstack/p9-jd <br/><br/><br/>

 <br/>
*********************
<br/>

## *Commande globale pour lancer le projet <br/>
Docker-compose up
<br/>
*********************
<br/><br/>
 <br/>
*********************
## *Commande pour lancer chaque micro-service*<br/>
Build: mvn clean install <br/>
Test: mvn test <br/>
Run: mvn spring-boot:run <br/>
*********************
<br/><br/>

##*Port des micro-service:*
mediscreen-discovery: Port 9090 <br/>
mediscreen-gataway: Port 9091 <br/>
mediscreen-doctor: Port 9004 <br/>
mediscreen-auth: Port 9002 <br/>
mediscreen-patient: Port 9092 <br/>
mediscreen-notes: Port 9095 <br/>
PhpMyAdmin: Port 8081
MongoDb: Port 27017
MediscreenBoard: Port 63001
*********************

