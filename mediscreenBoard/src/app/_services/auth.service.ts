import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const AUTH_API = 'http://localhost:9091/auth/';

   let httpOptions = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
    });

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }

  login(lastName: string, password: string): Observable<any> {
    return this.http.post('http://localhost:9091/auth/login', {
      lastName,
      password
      , httpOptions});
  }

  register(firstName: string,lastName: string, email: string, password: string): Observable<any> {
    return this.http.post('http://localhost:9091/auth', {
      firstName,
      lastName,
      email,
      password
      , httpOptions});
  }

  refreshToken(token: string) {
    return this.http.post(AUTH_API + 'refreshtoken', {
      refreshToken: token
      , httpOptions});
  }
}
