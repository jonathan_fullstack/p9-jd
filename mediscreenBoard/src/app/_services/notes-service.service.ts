import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Notes } from '../board-moderator/board-moderator.component';

const NOTES_API = 'http://localhost:9091/notes/';

const httpOption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

let httpOptions = new HttpHeaders({
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
});
@Injectable({
  providedIn: 'root'
})
export class NotesService {

  constructor(private http: HttpClient) { }

  createPatient(notes: Notes): Observable<any> {
    return this.http.post(NOTES_API + 'addNotes',
      notes , {headers: httpOptions});
  }

  getAllNotes(): Observable<any> {
    return this.http.get(NOTES_API + 'getAllNotes');
  }
}
