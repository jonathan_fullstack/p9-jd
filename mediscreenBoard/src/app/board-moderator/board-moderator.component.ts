import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { TokenStorageService } from '../_services/token-storage.service';
import { UserService } from '../_services/user.service';
import { EventBusService } from '../_shared/event-bus.service';
import { EventData } from '../_shared/event.class';
import { MatAccordion } from '@angular/material/expansion';
import { FormControl, FormGroup } from '@angular/forms';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { take } from 'rxjs/operators';
import { NotesService } from '../_services/notes-service.service';


export interface Patient {
  firstName?: string;
  lastName?: string;
  family?: string;
  dob?: Date;
  given?: Date;
  sex?: string;
  address?: string;
  phone?: string;
  note?: string;
  category?: string;
  createDate?: Date;
  updateDate?: Date;
}

export interface Notes {
  patientId?: number;
  id?: string;
  note?: string;
}

@Component({
  selector: 'app-board-moderator',
  templateUrl: './board-moderator.component.html',
  styleUrls: ['./board-moderator.component.css']
})
export class BoardModeratorComponent implements OnInit {
  panelOpenState = false;
  content?: string;
  pat?: Patient;
  user: any;
  patient: any;
  not: any;
  valid: boolean = true;
  form: any = {
    firstName: null,
    lastName: null,
    dob: null,
    sex: null,
    address: null,
    phone: null
  };
  patientForm = new FormGroup({
    firstName: new FormControl(),
    lastName: new FormControl(),
    dob: new FormControl(),
    sex: new FormControl(),
    address: new FormControl(),
    phone: new FormControl(),
  });

  notesForm = new FormGroup({
    notes: new FormControl()
  })

  statut: boolean = false;

  constructor(private userService: UserService,
    private tokenStorage: TokenStorageService,
    private notesService: NotesService) { }

  //   @ViewChild('autosize')
  // autosize!: CdkTextareaAutosize;


  ngOnInit(): void {
    this.user = this.tokenStorage.getUser()
    this.userService.getModeratorBoard().subscribe(
      data => {
        this.patient = data;
      },
      err => {
        this.content = err.error.message || err.error || err.message;
      }
    );

    this.notesService.getAllNotes().subscribe(
      data => {
        this.not = data;
      },
      err => {
        this.content = err.error.message || err.error || err.message;

      }
    );

    setTimeout(() => {
      this.userService.getModeratorBoard().subscribe(
        data => {
          this.patient = data;
        },
        err => {
          this.content = err.error.message || err.error || err.message;

        }
      );

    }, 1000);



  }

  cardTab(index: number) {

  }

  getListPatient() {
    (data: any) => {
      this.userService.getModeratorBoard().subscribe(
        data => {
          this.patient = data;
        },
        err => {
          this.content = err.error.message || err.error || err.message;

        }
      );
    }
  }

  onChangeEventDatePicker(e: any): void {
    this.form.dob === e;
  }

  onChangeEventSex(e: any): void {
    this.form.sex === e;
  }

  onFormSubmit() {

    let pat: Patient = {
      firstName: this.patientForm.get('firstName')?.value,
      lastName: this.patientForm.get('lastName')?.value,
      dob: this.patientForm.get('dob')?.value,
      address: this.patientForm.get('address')?.value,
      phone: this.patientForm.get('phone')?.value,
      sex: this.patientForm.get('sex')?.value,
    };
      for (let index = 0; index < this.patient.length; index++) {
        const element = this.patient[index];
        if (element.lastName === this.patientForm.get('lastName')?.value) {
          alert("this patient" + this.patientForm.get('lastName')?.value + "exist, change please" );
          this.valid === false;
          this.patientForm.get('lastName')?.reset;
        }
        if( this.patientForm.get('lastName')?.value === null) {
          alert("lastName is necessary please" );
          this.valid === false;
        }
        
      }
        if (this.valid === true) {
          this.userService.createPatient(pat).subscribe(
            {
              next: data => {
                window.location.reload();
                alert("patient add with success");
              },
              error:(e) => {
                if (e.status === "200" || e.status === 200 || e.code === 200 || e.code === '200' ) {
                  alert("patient add with success");
                   window.location.reload();
                } 
                else {
                  if (e.status === "400" || e.status === 400) {
                    alert("this patient exist in db");
                   window.location.reload();
                  } else {
                    window.location.reload();
                  }
                  
                }
              }
            }
          );
        }
   
  
    

  }
  EditForm(e: any) {
    this.notesForm.get('notes')?.setValue(e);
  }




  onSubmitNote(id: any) {
    let not: Notes = {
      patientId: id.toString(),
      id: id.toString(),
      note: this.notesForm.get('notes')?.value
    }


    this.notesService.createPatient(not).subscribe(
      {
        next: data => {
          window.location.reload();
        },
        error:(e) => {
          if (e.status === 200 || e.status === '200') {
            window.location.reload();
         }
        }
      }
    );
      setTimeout(() => {
        window.location.reload();
      }, 100);


  }
  reloadPage(): void {
    window.location.reload();
  }
}
