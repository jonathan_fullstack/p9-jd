import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    lastName: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  name: string = "";

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService,
    private router: Router) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  onSubmit(): void {
    const { lastName, password } = this.form;

    this.authService.login(lastName, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveRefreshToken(data.refreshToken);
        this.tokenStorage.saveUser(data.firstName);
        this.name = data.firstName;
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        if (this.tokenStorage.getUser()) {
          if (this.tokenStorage.getToken()) {
            setTimeout(() => {
              this.router.navigate(["/patient"]);
            }, 400);
          } else {
            
          }
          }
      //  this.roles = this.tokenStorage.getUser().roles;
       // this.reloadPage();
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
   
     

  }

  reloadPage(): void {
    window.location.reload();
  }
}
