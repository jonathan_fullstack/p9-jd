package mediscreenDoctor.mediscreenDoctor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import mediscreenDoctor.mediscreenDoctor.config.ExcludeFromJacocoGeneratedReport;

@SpringBootApplication
public class MediscreenDoctorApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediscreenDoctorApplication.class, args);
	}

	@ExcludeFromJacocoGeneratedReport
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
