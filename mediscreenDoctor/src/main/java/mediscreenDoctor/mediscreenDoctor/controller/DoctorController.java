package mediscreenDoctor.mediscreenDoctor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mediscreenDoctor.mediscreenDoctor.dto.LoginDto;
import mediscreenDoctor.mediscreenDoctor.entity.Doctor;
import mediscreenDoctor.mediscreenDoctor.service.UserService;

@RestController
@RequestMapping(value = "/users")
public class DoctorController {

	@Autowired
	private UserService userService;

	@PostMapping
	public Doctor save(@RequestBody Doctor user) {
		return userService.save(user);
	}

	@PostMapping("/login")
	public Doctor login(@RequestBody LoginDto login) {
    System.out.println(login);
    return userService.signin(login);
	}
}
