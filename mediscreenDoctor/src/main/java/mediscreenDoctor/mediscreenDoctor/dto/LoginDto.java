package mediscreenDoctor.mediscreenDoctor.dto;

import lombok.Data;

@Data
public class LoginDto {
	String lastName;
	String password;
}
