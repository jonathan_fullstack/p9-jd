package mediscreenDoctor.mediscreenDoctor.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mediscreenDoctor.mediscreenDoctor.entity.Doctor;

@Repository
public interface UserRepository extends JpaRepository<Doctor, Long> {

//	Optional<Doctor> findByLastName(String lastName);
	Doctor  findByLastName(String lastName);

}
