package mediscreenDoctor.mediscreenDoctor.service;

import lombok.extern.slf4j.Slf4j;
import mediscreenDoctor.mediscreenDoctor.dto.LoginDto;
import mediscreenDoctor.mediscreenDoctor.entity.Doctor;
import mediscreenDoctor.mediscreenDoctor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
@Slf4j
public class UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	RestTemplate restTemplate;

	public Doctor save(Doctor user) {
		Optional<Doctor> u = Optional.ofNullable(userRepository.findByLastName(user.getLastName()));
		if (u.isPresent()) {
			ResponseEntity.status(503).body("doctor exist in db");
			return null;
		} else {
			userRepository.save(user);
			ResponseEntity.ok("This doctor " + user.getLastName() + "add with success");
			return user;
		}

	}

	public Doctor signin(LoginDto login) {
    if (login.getLastName() != null && login.getPassword()  != null) {
      ResponseEntity.status(345).body("login is null");
    }
		Optional<Doctor> u = Optional.ofNullable(userRepository.findByLastName(login.getLastName()));
    if (u.get().getLastName() == null) {
      ResponseEntity.status(345).body("notUser");
    }
    return u.get();
	}

	public Doctor getById(long id) {
		return userRepository.findById(id).orElse(null);
	}
}
