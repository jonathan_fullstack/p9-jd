package mediscreenDoctor.mediscreenDoctor.value;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import mediscreenDoctor.mediscreenDoctor.entity.Doctor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseTemplateVO {

    private Doctor user;
}
