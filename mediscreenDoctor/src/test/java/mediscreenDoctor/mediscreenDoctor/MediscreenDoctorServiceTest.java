package mediscreenDoctor.mediscreenDoctor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import mediscreenDoctor.mediscreenDoctor.dto.LoginDto;
import mediscreenDoctor.mediscreenDoctor.entity.Doctor;
import mediscreenDoctor.mediscreenDoctor.repository.UserRepository;
import mediscreenDoctor.mediscreenDoctor.service.UserService;

@ExtendWith(MockitoExtension.class)
public class MediscreenDoctorServiceTest {

	@Spy
	@InjectMocks
	UserService userService;

	@Mock
	UserRepository userRepository;

	/**
	 * @Description method for save user with success
	 */
	@Test
	public void saveUserWithSuccess() {
		// GIVEN
		Doctor user = new Doctor();
		user.setLastName("ggg");
		user.setFirstName("gggg");
		user.setEmail("dddf@hh.com");
		user.setPassword("htgfcf@@hgH");
		user.setRole("admin");

		// WHEN
		lenient().when(userRepository.save(any())).thenReturn(user);
		// THEN
		userService.save(user);
		verify(userService).save(user);
	}

	/**
	 * @Description method for signin user with success
	 */
	@Test
	public void signinWithSuccess() {
		// GIVEN
		Doctor user = new Doctor();
		user.setLastName("ggg");
		user.setFirstName("gggg");
		user.setEmail("dddf@hh.com");
		user.setPassword("htgfcf@@hgH");
		user.setRole("admin");
		LoginDto login = new LoginDto();
		login.setLastName("tftdrr");
		login.setPassword("tddrr");

		// WHEN
		lenient().when(userRepository.findByLastName(anyString())).thenReturn(user);
		// THEN
		userService.signin(login);
		verify(userService).signin(login);
	}

	/**
	 * @Description method for get user with success
	 */
	@Test
	public void getUserWithSuccess() {
		// GIVEN
		Doctor user = new Doctor();
		user.setLastName("ggg");
		user.setFirstName("gggg");
		user.setEmail("dddf@hh.com");
		user.setPassword("htgfcf@@hgH");
		user.setRole("admin");
		LoginDto login = new LoginDto();
		login.setLastName("tftdrr");
		login.setPassword("tddrr");

		// WHEN
		lenient().when(userRepository.findById(anyLong())).thenReturn(Optional.of(user));
		// THEN
		userService.getById((long) 18);
		verify(userService).getById((long) 18);
	}

}
