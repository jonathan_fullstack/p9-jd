package mediscreenDoctor.mediscreenDoctor.controllerTest;



import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mediscreenDoctor.mediscreenDoctor.controller.DoctorController;
import mediscreenDoctor.mediscreenDoctor.dto.LoginDto;
import mediscreenDoctor.mediscreenDoctor.entity.Doctor;
import mediscreenDoctor.mediscreenDoctor.repository.UserRepository;
import mediscreenDoctor.mediscreenDoctor.service.UserService;



@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

	private MockMvc mockMvc;
	
	@InjectMocks
	DoctorController userController;
	
	@Mock
	UserService userService;
	
	@Mock
	UserRepository userRepository;
	
	@BeforeEach
	private void setUpPerTest() {
		mockMvc = MockMvcBuilders.standaloneSetup(userController).build();

	}
	
	
	/**
	 * @throws Exception
	 * @Description test method controller  for save user
	 */
	@Test
	public void saveUser() {
		//GIVEN
		LocalDate date = LocalDate.now();
		Doctor user = new Doctor();
		user.setLastName("ggg");
		user.setFirstName("gggg");
		user.setEmail("dddf@hh.com");
		user.setPassword("htgfcf@@hgH");
		user.setRole("admin");
		//WHEN
		lenient().when(userController.save(any())).thenReturn(user);
		//ResponseEntity<String> responseEntity = userController.save(user);
        
		assertEquals(userController.save(user), user);
	//	assertEquals(responseEntity.getHeaders().getLocation().getPath()).isEqualTo("/1");
	}
	
	/**
	 * @throws Exception
	 * @Description test method controller  for login user
	 */
	@Test
	public void loginTest() {
		//GIVEN
		Doctor user = new Doctor();
		user.setLastName("ggg");
		user.setFirstName("gggg");
		user.setEmail("dddf@hh.com");
		user.setPassword("htgfcf@@hgH");
		user.setRole("admin");
		LoginDto login = new LoginDto();
		login.setLastName("tftdrr");
		login.setPassword("tddrr");
		//WHEN
		lenient().when(userService.signin(any())).thenReturn(user);
		assertEquals(userController.login(login), user);
	}
	
	
}
