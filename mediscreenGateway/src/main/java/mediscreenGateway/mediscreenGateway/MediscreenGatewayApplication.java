package mediscreenGateway.mediscreenGateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MediscreenGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediscreenGatewayApplication.class, args);
		System.out.println("version 2.1.0");
	}

}
