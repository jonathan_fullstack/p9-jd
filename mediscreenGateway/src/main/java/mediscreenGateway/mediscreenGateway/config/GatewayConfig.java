package mediscreenGateway.mediscreenGateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableHystrix
public class GatewayConfig {

    @Autowired
    AuthenticationFilter filter;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("mediscreen-doctor", r -> r.path("/users/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://mediscreen-doctor:9002"))

                .route("mediscreen-security", r -> r.path("/auth/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://mediscreen-security:9004/"))
                .route("mediscreen-patient", r -> r.path("/patient/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://mediscreen-patient:9092/"))
                .route("mediscreen-notes", r -> r.path("/notes/**")
                        .filters(f -> f.filter(filter))
                        .uri("http://mediscreen-notes:9095/"))
                .build();

    }

}
