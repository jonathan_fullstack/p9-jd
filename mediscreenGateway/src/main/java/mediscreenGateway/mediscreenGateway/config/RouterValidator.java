package  mediscreenGateway.mediscreenGateway.config;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Component
public class RouterValidator {



	public ArrayList<String> newListMethod() {
		ArrayList<String> newList = new ArrayList<>();
		newList.add( "/auth/login");
    newList.add( "/users/login");
    newList.add( "/auth/login");
    newList.add( "/auth");
    newList.add( "/users");
		return newList;
	}

//    public static final List<String> openApiEndpoints = List.of(
//            "/auth/register",
//            "/auth/login",
//            "/auth/test"
//    );



    public Predicate<ServerHttpRequest> isSecured =
            request -> newListMethod()
                    .stream()
                    .noneMatch(uri -> request.getURI().getPath().contains(uri));

}

