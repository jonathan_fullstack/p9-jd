package mediscreenNotes.mediscreenNotes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import mediscreenNotes.mediscreenNotes.config.ExcludeFromJacocoGeneratedReport;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class MediscreenNotesApplication {

	@ExcludeFromJacocoGeneratedReport
	public static void main(String[] args) {
		SpringApplication.run(MediscreenNotesApplication.class, args);
	}

}
