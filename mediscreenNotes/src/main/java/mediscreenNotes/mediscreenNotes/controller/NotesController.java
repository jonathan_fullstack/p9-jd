package mediscreenNotes.mediscreenNotes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mediscreenNotes.mediscreenNotes.entity.NotesEntity;
import mediscreenNotes.mediscreenNotes.repository.NotesRepository;
import mediscreenNotes.mediscreenNotes.services.NotesCrudServices;

@RestController
@RequestMapping("/notes")
public class NotesController {

	@Autowired
	NotesCrudServices notesService;

	@Autowired
	NotesRepository notesRepository;

	/**
	 * @Description method when return this create or update notes
	 * @param notes
	 * @return
	 */
	@PostMapping("/addNotes")
	public ResponseEntity<String> addNotes(@RequestBody NotesEntity notes) {
		return notesService.saveNotes(notes);
	}

	/**
	 * @Description method when return this deletes notes
	 * @param notes
	 * @return
	 */
	@PostMapping("/deleteNotes")
	public ResponseEntity<String> deletesNotes(@RequestBody NotesEntity notes) {
		return notesService.deleteNotes(notes);
	}

	@GetMapping("/getAllNotes")
	public List<NotesEntity> getAllNotes() {
		return notesRepository.findAll();
	}

	@PostMapping("/getById/{patientId}")
	public NotesEntity getById(@PathVariable("patientId") String patientId) {
		return notesService.notesByPatient(patientId);
	}

}
