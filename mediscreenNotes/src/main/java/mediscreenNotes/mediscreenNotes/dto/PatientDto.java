package mediscreenNotes.mediscreenNotes.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class PatientDto {
	long id;
	String firstName;
	String lastName;
	LocalDate dob;
	String sex;
	String address;
	String phone;
	String note;
	String category;
	LocalDate createDate;
	LocalDate updateDate;
}
