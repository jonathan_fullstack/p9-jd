package mediscreenNotes.mediscreenNotes.entity;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "notes")
public class NotesEntity {
	@Id
	public String id;
	@Indexed(unique = true)
	public String patientId;
	public String note;
	public String category;
	public LocalDate createDate;
	public LocalDate updateDate;

}
