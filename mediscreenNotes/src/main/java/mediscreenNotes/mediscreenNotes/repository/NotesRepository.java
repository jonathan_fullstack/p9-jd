package mediscreenNotes.mediscreenNotes.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import mediscreenNotes.mediscreenNotes.entity.NotesEntity;

@Repository
public interface NotesRepository extends MongoRepository<NotesEntity, String> {

	Optional<NotesEntity> findByPatientId(String patientId);
	
	@Query("{note:'?0'}")
    String findItemByName(String note);

	Optional<String> findByNote(String string);

}
