package mediscreenNotes.mediscreenNotes.services;

import java.time.LocalDate;
import java.time.Period;
import java.util.Locale;
import java.util.Optional;

import org.bouncycastle.crypto.RuntimeCryptoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mediscreenNotes.mediscreenNotes.config.ExcludeFromJacocoGeneratedReport;
import mediscreenNotes.mediscreenNotes.dto.PatientDto;
import mediscreenNotes.mediscreenNotes.entity.NotesEntity;
import mediscreenNotes.mediscreenNotes.repository.NotesRepository;

@Service
public class NotesCrudServices {

	@Autowired
	NotesRepository notesRepository;

	@Autowired
	PatientFeignService patientFeignService;

	/**
	 * @Description method for create or update notes of patient
	 * @param notes
	 * @return
	 */
	@ExcludeFromJacocoGeneratedReport
  public ResponseEntity<String> saveNotes(NotesEntity notes) {
    Optional<NotesEntity> not = (Optional<NotesEntity>) notesRepository.findByPatientId(notes.getPatientId());
    LocalDate date = LocalDate.now();
    NotesEntity noteToSave = new NotesEntity();
    if (!not.isPresent()) {
      noteToSave.setId(notes.getId());
      noteToSave.setPatientId(notes.getPatientId());
      noteToSave.setNote(notes.getNote());
      noteToSave.setCreateDate(date);
      noteToSave = attributeCategory(noteToSave);
      notesRepository.insert(noteToSave);
      return ResponseEntity.ok("notes is save");
    } else {
      notes.setUpdateDate(date);
      updateNotes(attributeCategory(notes));
      return ResponseEntity.ok("notes is update");
    }
  }


	/**
	 * @Description method for update notes of patient
	 * @param notes
	 * @return
	 */
	public ResponseEntity<String> updateNotes(NotesEntity notes) {
    Optional<NotesEntity> not = (Optional<NotesEntity>) notesRepository.findByPatientId(notes.getPatientId());
    if (not.isPresent()) {
      LocalDate date = LocalDate.now();
      not.get().setNote(notes.getNote());
      not.get().setUpdateDate(date);
      notesRepository.save(attributeCategory(not.get()));
      return ResponseEntity.ok("notes is update");
    } else {
      return ResponseEntity.status(504).body("notes not found");
    }
  }



  /**
	 * @Description method for delete notes of patient
	 * @param notes
	 * @return
	 */
	public ResponseEntity<String> deleteNotes(NotesEntity notes) {
		Optional<NotesEntity> not = notesRepository.findById(notes.getId());
		if (not.isPresent()) {
			notesRepository.delete(notes);
			return ResponseEntity.ok("notes is delete");
		} else {
			return ResponseEntity.status(504).body("notes not found");
		}
	}

	/**
	 * @Description method for get note of patient with parameters patientId, if
	 *              notes is null, one notes with default is created
	 * @param patientId
	 * @return
	 */
	public NotesEntity notesByPatient(String patientId) {
		Optional<NotesEntity> not = notesRepository.findByPatientId(patientId);
		if (not.isPresent()) {
			return not.get();
		} else {
			LocalDate date = LocalDate.now();
			NotesEntity newNotes = new NotesEntity();
			newNotes.setPatientId(patientId);
			newNotes.setCreateDate(date);
			newNotes.setNote("not note actual");
			newNotes = attributeCategory(newNotes);
			notesRepository.insert(newNotes);
			return newNotes;
		}
	}

	/**
	 * @Description method for attribute category since notes of patient, this
	 *              parameters is define for atrribute category, since this number,
	 *              one pamaters is attribute,in default the none paramteris is
	 *              attribute
	 * @param notes
	 * @return
	 */
	public NotesEntity attributeCategory(NotesEntity notes) {
		Optional<PatientDto> patDto = Optional
				.ofNullable(patientFeignService.getPatientById(Integer.parseInt(notes.getPatientId())));
		// compose note string in array string
		String[] wordts = notes.getNote().split(" ");
		int total = 0;
		for (String word : wordts) {
			if (word.toLowerCase(Locale.ROOT).indexOf("hémoglobine a1c") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("microalbumine") != -1) {
				++total;
			}

			if (word.toLowerCase(Locale.ROOT).indexOf("taille") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("poids") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("anormal") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("cholestérol") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("vertige") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("rechute") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("réaction") != -1) {
				++total;
			}
			if (word.toLowerCase(Locale.ROOT).indexOf("anticorps") != -1) {
				++total;
			}

		}
		// calcul age of patient
		int age = calculateAge(patDto.get());
		if (total >= 8 && age > 30) {
			notes.setCategory("Early onset");
			System.out.println("m8");
			return notes;
		} else {
			if (total  >= 7 && age < 30 && patDto.get().getSex().contains("w")) {
				notes.setCategory("Early onset");
				System.out.println("m7");
				return notes;
			} else {
          if (total >= 6 && age > 30 ) {
            notes.setCategory("In Danger");
            System.out.println("m4");
            return notes;
          } else {
            if (total == 5 && age < 30 && patDto.get().getSex().contains("m")) {
              notes.setCategory("Early onset");
              System.out.println("m5");
              return notes;
            } else {
              if (total >= 4 && age < 30 && patDto.get().getSex().contains("w")) {
                notes.setCategory("In Danger");
                System.out.println("m4");
                return notes;
              } else {
                if (total >= 3 && age < 30 && patDto.get().getSex().contains("m")) {
                  notes.setCategory("In Danger");
                  System.out.println("m3");
                  return notes;
                } else {
                  if (total <= 2 && age < 30) {
                    System.out.println("m1");
                    notes.setCategory("None");
                    return notes;
                  } else {
                  if (total >= 1 && age > 30) {
                    notes.setCategory("Borderline");
                    System.out.println("m2");
                    return notes;
                  } else {
                      if (total < 1) {
                        System.out.println("m1");
                        notes.setCategory("None");
                        return notes;
                      } else {
                        System.out.println("m1");
                        if (notes.getCategory() == null) {
                          notes.setCategory("None");
                          return notes;
                        }
                        return notes;
                      }
                    }
                  }
                }
              }
            }
          }
			}
		}

	}

	/**
	 * @Description method for calcul age since date of now and birthday of patient
	 * @param patient
	 * @return
	 */
	public int calculateAge(PatientDto patient) {
		if (patient != null) {
			LocalDate dateNow = LocalDate.now();
			return Period.between(patient.getDob(), dateNow).getYears();
		} else {
			throw new RuntimeCryptoException("Patient is not found");
		}

	}
}
