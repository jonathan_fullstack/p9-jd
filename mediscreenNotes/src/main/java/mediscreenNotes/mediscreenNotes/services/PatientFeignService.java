package mediscreenNotes.mediscreenNotes.services;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mediscreenNotes.mediscreenNotes.dto.PatientDto;

@FeignClient(name = "mediscreenPatient", url = "host.docker.internal:9092")
public interface PatientFeignService {

	/**
	 * @Description method when return this patientDto
	 * @param id
	 * @return
	 */
	@PostMapping("patient/getPatientWithPatientId")
	public PatientDto getPatientById(@RequestBody int id);
}
