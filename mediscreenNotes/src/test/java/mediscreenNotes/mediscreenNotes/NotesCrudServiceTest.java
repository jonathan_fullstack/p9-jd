package mediscreenNotes.mediscreenNotes;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import mediscreenNotes.mediscreenNotes.dto.PatientDto;
import mediscreenNotes.mediscreenNotes.entity.NotesEntity;
import mediscreenNotes.mediscreenNotes.repository.NotesRepository;
import mediscreenNotes.mediscreenNotes.services.NotesCrudServices;
import mediscreenNotes.mediscreenNotes.services.PatientFeignService;

@ExtendWith(MockitoExtension.class)
public class NotesCrudServiceTest {

	@Spy
	@InjectMocks
	NotesCrudServices notesCrudService;

	@Mock
	NotesRepository notesRepository;

	@Mock
	PatientFeignService patientFeignService;

	/**
	 * @Description method for update notes with success
	 */
	@Test
	public void updateNotesWithSuccess() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		// WHEN
		lenient().when(notesRepository.findByPatientId(anyString())).thenReturn(Optional.of(note));
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// lenient().when(notesRepository.save(any())).thenReturn(Optional.of(note));
		// THEN
		notesCrudService.updateNotes(note);
		verify(notesCrudService).updateNotes(note);
	}

	/**
	 * @Description method for deletes notes with success
	 */
	@Test
	public void deletesNotesWithSuccess() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		// WHEN
		lenient().when(notesRepository.findByPatientId(anyString())).thenReturn(null);
		// THEN
		notesCrudService.deleteNotes(note);
		verify(notesCrudService).deleteNotes(note);
	}

	/**
	 * @Description method for get notes with patientId with success
	 */
	@Test
	public void getNotesWithSuccess() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		// WHEN
		lenient().when(notesRepository.findByPatientId(anyString())).thenReturn(Optional.of(note));
		// THEN
		notesCrudService.notesByPatient(note.getPatientId());
		verify(notesCrudService).notesByPatient(note.getPatientId());
	}

	/**
	 * @Description method for get notes with patientId with success
	 */
	@Test
	public void getNotesWithSuccess2() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();

		// WHEN
		lenient().when(notesRepository.findByPatientId(anyString())).thenReturn(Optional.of(note));
		// THEN
		notesCrudService.notesByPatient("36");
		verify(notesCrudService).notesByPatient("36");
	}

	/**
	 * @Description method for attribute cataegory with success
	 */
	@Test
	public void attributeCategoryWithSuccess() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("anticorps de réaction");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(1960, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		// lenient().when(patientFeignService.getPatientById(any())).thenReturn(patientDto);
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// THEN
		notesCrudService.attributeCategory(note);
		verify(notesCrudService).attributeCategory(note);
	}

	/**
	 * @Description method for attribute cataegory with success
	 */
	@Test
	public void attributeCategoryWithSuccess2() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("anticorps réaction rechute vertige cholestérol anormal");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(1960, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		// lenient().when(patientFeignService.getPatientById(any())).thenReturn(patientDto);
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// THEN
		notesCrudService.attributeCategory(note);
		verify(notesCrudService).attributeCategory(note);
	}

	/**
	 * @Description method for attribute cataegory with success
	 */
	@Test
	public void attributeCategoryWithSuccess3() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("anticorps réaction rechute vertige cholestérol");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(1960, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// THEN
		notesCrudService.attributeCategory(note);
		verify(notesCrudService).attributeCategory(note);
	}

	/**
	 * @Description method for attribute cataegory with success
	 */
	@Test
	public void attributeCategoryWithSuccess4() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("anticorps réaction");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(1960, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// THEN
		notesCrudService.attributeCategory(note);
		verify(notesCrudService).attributeCategory(note);
	}

	/**
	 * @Description method for attribute cataegory with success
	 */
	@Test
	public void attributeCategoryWithSuccess5() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("anticorps réaction cholestérol");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(2005, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// THEN
		notesCrudService.attributeCategory(note);
		verify(notesCrudService).attributeCategory(note);
	}

	/**
	 * @Description method for attribute cataegory with success
	 */
	@Test
	public void attributeCategoryWithSuccess6() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("anticorps réaction cholestérol rechute");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("w");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(2005, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// THEN
		notesCrudService.attributeCategory(note);
		verify(notesCrudService).attributeCategory(note);
	}

	/**
	 * @Description method for attribute cataegory with success
	 */
	@Test
	public void attributeCategoryWithSuccess7() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("anticorps réaction cholestérol rechute rechute rechute rechute rechute");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("w");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(1954, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		Mockito.when(patientFeignService.getPatientById(anyInt())).thenReturn(patientDto);
		// THEN
		notesCrudService.attributeCategory(note);
		verify(notesCrudService).attributeCategory(note);
	}

	/**
	 * @Description method for calculateAge with success
	 */
	@Test
	public void calculateAgeWithSuccess() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("w");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(LocalDate.of(2005, 1, 1));
		patientDto.setLastName("test family");
		patientDto.setCreateDate(date);
		patientDto.setUpdateDate(date);
		// WHEN
		RuntimeException exception = assertThrows(RuntimeException.class, () -> {
			notesCrudService.calculateAge(null);
		});
		// THEN
		assertEquals("Patient is not found", exception.getMessage());
	}
}
