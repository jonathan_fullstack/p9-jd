package mediscreenNotes.mediscreenNotes.controllerTest;



import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mediscreenNotes.mediscreenNotes.controller.NotesController;
import mediscreenNotes.mediscreenNotes.entity.NotesEntity;
import mediscreenNotes.mediscreenNotes.repository.NotesRepository;
import mediscreenNotes.mediscreenNotes.services.NotesCrudServices;


@ExtendWith(MockitoExtension.class)
public class NotesControllerTest {

	private MockMvc mockMvc;
	
	@InjectMocks
	NotesController noteController;
	
	@Mock
	NotesCrudServices notesCrudService;
	
	@Mock
	NotesRepository noteRepository;
	
	@BeforeEach
	private void setUpPerTest() {
		mockMvc = MockMvcBuilders.standaloneSetup(noteController).build();

	}
	
	
	/**
	 * @throws Exception
	 * @Description test method controller  for add new  notes ou update notes
	 */
	@Test
	public void addNotesIdTest() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		// THEN
		try {
			mockMvc.perform(post("/notes/addNotes").accept(MediaType.APPLICATION_JSON).contentType(MEDIA_TYPE_JSON_UTF8)
					.content(objectMapper.writeValueAsString(note))).andDo(print()).andExpect(status().isOk());
			assertEquals(notesCrudService.saveNotes(note), ResponseEntity.ok());
			notesCrudService.saveNotes(note);
			verify(noteController).addNotes(note);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @throws Exception
	 * @Description test method controller  for add new  notes ou update notes2
	 */
	@Test
	public void addNotesIdTest2() {
		//GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		//WHEN
		lenient().when(notesCrudService.saveNotes(any())).thenReturn(ResponseEntity.ok("notes is save"));
		ResponseEntity<String> responseEntity = noteController.addNotes(note);
        
		assertEquals(responseEntity.getStatusCodeValue(), 200);
	//	assertEquals(responseEntity.getHeaders().getLocation().getPath()).isEqualTo("/1");
	}
	
	/**
	 * @throws Exception
	 * @Description test method controller  for delete notes
	 */
	@Test
	public void deletePatientIdTest() {
		// GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json", java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		// THEN
		try {
			mockMvc.perform(post("/notes/deleteNotes").accept(MediaType.APPLICATION_JSON).contentType(MEDIA_TYPE_JSON_UTF8)
					.content(objectMapper.writeValueAsString(note))).andDo(print()).andExpect(status().isOk());
			assertEquals(notesCrudService.deleteNotes(note), ResponseEntity.ok());
			verify(noteController).deletesNotes(note);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @throws Exception
	 * @Description test method controller  for delete notes2
	 */
	@Test
	public void deleteNotesIdTest2() {
		//GIVEN
		LocalDate date = LocalDate.now();
		NotesEntity note = new NotesEntity();
		note.setCategory("None");
		note.setNote("fff");
		note.setPatientId("18");
		note.setCreateDate(date);
		note.setUpdateDate(date);
		//WHEN
		lenient().when(notesCrudService.deleteNotes(any())).thenReturn(ResponseEntity.ok("notes is save"));
		ResponseEntity<String> responseEntity = noteController.deletesNotes(note);
        
		assertEquals(responseEntity.getStatusCodeValue(), 200);
	//	assertEquals(responseEntity.getHeaders().getLocation().getPath()).isEqualTo("/1");
	}
	
	/**
	 * @throws Exception
	 * @Description test fot get all notes
	 */
	@Test
	public void getListNotesSucces() throws Exception {
		// GIVEN
		List<NotesEntity> listeNotes = new ArrayList<>();
		  mockMvc.perform(get("/notes/getAllNotes") 
		          .accept(MediaType.APPLICATION_JSON));
		  assertEquals(noteRepository.findAll(), listeNotes);
			
		
	}
}
