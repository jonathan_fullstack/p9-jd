package mediscreenPatient.mediscreenPatient;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import feign.codec.Decoder;
import mediscreenPatient.mediscreenPatient.config.ExcludeFromJacocoGeneratedReport;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@EnableJpaRepositories(basePackages = { "mediscreenPatient.mediscreenPatient" })
@ComponentScan("mediscreenPatient.mediscreenPatient")
public class MediscreenPatientApplication {
	@ExcludeFromJacocoGeneratedReport
	public static void main(String[] args) {
		SpringApplication.run(MediscreenPatientApplication.class, args);
	}

//	@ExcludeFromJacocoGeneratedReport
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//		return new WebMvcConfigurer() {
//			@ExcludeFromJacocoGeneratedReport
//			@Override
//			public void addCorsMappings(CorsRegistry registry) {
//				registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("PUT", "DELETE",
//						"GET", "POST");
//			}
//		};
//	}

	
	private org.springframework.beans.factory.ObjectFactory<HttpMessageConverters> messageConverters = HttpMessageConverters::new;

	@ExcludeFromJacocoGeneratedReport
	@Bean
	Decoder feignFormDecoder() {
		return new SpringDecoder(messageConverters);
	}

	@ExcludeFromJacocoGeneratedReport
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
