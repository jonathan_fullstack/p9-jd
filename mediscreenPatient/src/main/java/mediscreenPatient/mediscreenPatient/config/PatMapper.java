package mediscreenPatient.mediscreenPatient.config;

import java.util.List;

import org.mapstruct.Mapper;

import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.entity.Patient;

@Mapper
public interface PatMapper {
Patient toPatient(PatientDto patientDto);
PatientDto toPatientDto(Patient patient);

List<Patient> toPatientList(List<PatientDto> patientDto);
List<PatientDto> toPatientDtoList(List<Patient> patient);
}
