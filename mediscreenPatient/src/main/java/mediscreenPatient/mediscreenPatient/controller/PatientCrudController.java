package mediscreenPatient.mediscreenPatient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.service.PatientCrudService;

@RestController
@RequestMapping("/patient")
public class PatientCrudController {

	@Autowired
	PatientCrudService patientCrudService;


	/**
	 * @Description method when return this create patient service
	 * @param patientDto
	 * @return
	 */
	@PostMapping("/addPatient")
	public ResponseEntity<String> addPatient(@RequestBody PatientDto patientDto) {
		return patientCrudService.createPatient(patientDto);
	}

	/**
	 * @Description method when return this update patient service
	 * @param patientDto
	 * @return
	 */
	@PostMapping("/updatePatient")
	public ResponseEntity<String> updatePatient(@RequestBody PatientDto patientDto) {
		return patientCrudService.updatePatient(patientDto);
	}

	/**
	 * @Description method when return this delete patient service
	 * @param patientDto
	 * @return
	 */
	@PostMapping("/deletePatient")
	public ResponseEntity<String> deletePatient(@RequestBody PatientDto patientDto) {
		return patientCrudService.deletePatient(patientDto);
	}

}
