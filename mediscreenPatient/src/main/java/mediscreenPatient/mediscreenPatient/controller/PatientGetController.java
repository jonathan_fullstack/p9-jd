package mediscreenPatient.mediscreenPatient.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.service.PatientGetService;

@RestController
@RequestMapping("/patient")
public class PatientGetController {

	@Autowired
	PatientGetService patientGetService;

	/**
	 * @Description method when return this get list patient service
	 * @return
	 */
	@GetMapping("/listPatient")
	public List<PatientDto> listOfAllPatient() {
		return patientGetService.listOfAllPatient();
	}

	/**
	 * @Description method when return this get patient service
	 * @param patientDto
	 * @return
	 */
	@PostMapping("/getPatientWithFamily")
	public ResponseEntity<PatientDto> getPatient(@RequestBody PatientDto patientDto) {
		return patientGetService.getPatient(patientDto);
	}

	/**
	 * @Description method when return this patientDto
	 * @param id
	 * @return
	 */
	@PostMapping("/getPatientWithPatientId")
	public PatientDto getPatientById(@RequestBody int id) {
		return patientGetService.getPatientByPatientId(id);
	}
}
