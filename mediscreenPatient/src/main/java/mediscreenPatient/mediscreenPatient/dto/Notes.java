package mediscreenPatient.mediscreenPatient.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class Notes {
	private String patientId;
	private String note;
	private String category;
	private LocalDate createDate;
	private LocalDate updateDate;

	public void setNotes(String note) {
		this.note = note;
	}

}
