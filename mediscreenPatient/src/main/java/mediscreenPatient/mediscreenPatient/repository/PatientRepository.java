package mediscreenPatient.mediscreenPatient.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mediscreenPatient.mediscreenPatient.entity.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> { 
	Patient findByLastName(String lastName);
}
