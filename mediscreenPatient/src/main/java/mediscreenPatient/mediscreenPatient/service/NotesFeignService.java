package mediscreenPatient.mediscreenPatient.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import mediscreenPatient.mediscreenPatient.dto.Notes;

@FeignClient(name = "mediscreen-notes", url = "host.docker.internal:9095")
public interface NotesFeignService {

	@PostMapping("notes/getById/{patientId}")
	public Notes getById(@PathVariable("patientId") String patientId);
}
