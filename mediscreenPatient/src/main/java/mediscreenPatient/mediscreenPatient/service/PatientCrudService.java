package mediscreenPatient.mediscreenPatient.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.entity.Patient;
import mediscreenPatient.mediscreenPatient.repository.PatientRepository;

@Service
public class PatientCrudService {

	@Autowired
	PatientRepository patientRepository;



	/**
	 * @Description method for create new patient with verify than patient is not
	 *              exist in db
	 * @param patientDto
	 * @return
	 */
	public ResponseEntity<String> createPatient(PatientDto patientDto) {
		Patient pat = patientRepository.findByLastName(patientDto.getLastName());
		if (pat == null) {
			Patient patient = new Patient();
			patient.setLastName(patientDto.getLastName());
			patient.setFirstName(patientDto.getFirstName());
			patient.setAddress(patientDto.getAddress());
			patient.setSex(patientDto.getSex());
			patient.setDob(patientDto.getDob());
			patient.setPhone(patientDto.getPhone());
			patientRepository.save(patient);
			return ResponseEntity.ok("new patient:" + patientDto + "is create");

		} else {
			return ResponseEntity.badRequest().body("Patient exist in db");
		}

	}

	public LocalDate convertToLocalDateTimeViaInstant(LocalDate localDateTime) {
		return localDateTime.now();
	}

	/**
	 * @Description method for update patient with this id
	 * @param patientDto
	 * @return
	 */
	public ResponseEntity<String> updatePatient(PatientDto patientDto) {
		Optional<Patient> pat = Optional.ofNullable(patientRepository.findByLastName(patientDto.getLastName()));
		if (pat.isPresent()) {
			patientRepository.save(pat.get());
			return ResponseEntity.ok(" patient:" + patientDto + "is update");

		} else {
			return ResponseEntity.badRequest().body("Not patient found");
		}
	}

	/**
	 * @Description method for delete patient with this id
	 * @param patientDto
	 * @return
	 */
	public ResponseEntity<String> deletePatient(PatientDto patientDto) {
		Optional<Patient> pat = Optional.ofNullable(patientRepository.findByLastName(patientDto.getLastName()));
		pat = patientRepository.findById(pat.get().getId());
		if (pat.isPresent()) {
			patientRepository.delete(pat.get());
			return ResponseEntity.ok(" patient:" + patientDto + "is delete");

		} else {
			return ResponseEntity.badRequest().body("Not patient found");
		}
	}
}
