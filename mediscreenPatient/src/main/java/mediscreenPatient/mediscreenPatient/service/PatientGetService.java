package mediscreenPatient.mediscreenPatient.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mediscreenPatient.mediscreenPatient.config.ExcludeFromJacocoGeneratedReport;
import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.entity.Patient;
import mediscreenPatient.mediscreenPatient.repository.PatientRepository;
import mediscreenPatient.mediscreenPatient.utils.PatientMapper;
import org.springframework.web.client.RestTemplate;

@Service
public class PatientGetService {

	@Autowired
	PatientMapper patientMapper;

	@Autowired
	PatientRepository patientRepository;

	@Autowired
	NotesFeignService notesFeignServices;

	@Autowired
	private ModelMapper modelMapper;


	/***
	 * @Description method for findAllPatient
	 * @return
	 */
	@ExcludeFromJacocoGeneratedReport
	public List<PatientDto> listOfAllPatient() {
		if (patientRepository.findAll().size() < 0) {
			ResponseEntity.badRequest().body("Not list of patient");
		}

		// List<PatientDto> listPatDto = new ArrayList<>();
		List<PatientDto> listPatDto = patientRepository.findAll().stream()
				.map(user -> modelMapper.map(user, PatientDto.class)).collect(Collectors.toList());

		listPatDto.stream()
				.map(n -> n.getNote() == returnNotes(n.getId()) && n.getCategory() == returnCategory(n.getId())
						&& n.getCreateDate() == returnCreatedDate(n.getId())
						&& n.getUpdateDate() == returnUpdateDate(n.getId()))
				.collect(Collectors.toList());

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);


		for (PatientDto p : listPatDto) {
      String str = String.valueOf(p.getId());
      p.setNote(notesFeignServices.getById(str).getNote());
      p.setCategory(notesFeignServices.getById(str).getCategory());
      p.setCreateDate(notesFeignServices.getById(str).getCreateDate());
      p.setUpdateDate(notesFeignServices.getById(str).getUpdateDate());
		}
		return listPatDto;
	}

	/**
	 * @Method for return string note from note micro-service
	 * @param PatientId
	 * @return
	 */
	public String returnNotes(Long PatientId) {
		String strNotes = String.valueOf(PatientId);
		return notesFeignServices.getById(strNotes).getNote();
	}

	/**
	 * @Method for return string category from note micro-service
	 * @param PatientId
	 * @return
	 */
	public String returnCategory(Long PatientId) {
		String strCategory = String.valueOf(PatientId);
		return notesFeignServices.getById(strCategory).getCategory();
	}

	/**
	 * @Method for return localdate date from note micro-service
	 * @param PatientId
	 * @return
	 */
	public LocalDate returnCreatedDate(Long PatientId) {
		String strCreatedDate = String.valueOf(PatientId);
		return notesFeignServices.getById(strCreatedDate).getCreateDate();
	}

	/**
	 * @Method for return localdate date from note micro-service
	 * @param PatientId
	 * @return
	 */
	public LocalDate returnUpdateDate(Long PatientId) {
		String strUpdateDate = String.valueOf(PatientId);
		return notesFeignServices.getById(strUpdateDate).getUpdateDate();
	}

	/**
	 * @Description method for get patient from db with lastNameParameters
	 * @param patientDto
	 * @return
	 */
	public ResponseEntity<PatientDto> getPatient(PatientDto patientDto) {
		Optional<Patient> pat = Optional.ofNullable(patientRepository.findByLastName(patientDto.getLastName()));
		if (pat.isPresent()) {
			return ResponseEntity.ok(patientMapper.patientToPatientDto(pat.get()));

		} else {
			return (ResponseEntity<PatientDto>) ResponseEntity.badRequest();

		}

	}

	/**
	 * @Description metod for get patient from db, use modelMapper for convert
	 *              entity in dto
	 * @param patientId
	 * @return
	 */
	public PatientDto getPatientByPatientId(long patientId) {
		Optional<Patient> pat = patientRepository.findById(patientId);
		if (pat.isPresent()) {
			PatientDto pdto = modelMapper.map(pat.get(), PatientDto.class);
			return pdto;
		} else {
			ResponseEntity.noContent();
			return null;
		}
	}

}
