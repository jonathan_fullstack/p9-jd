package mediscreenPatient.mediscreenPatient.utils;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.entity.Patient;

@Component
@Mapper(uses = { PatientMapper.class }, componentModel = "spring")
public interface PatientMapper {
	PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);

	public Patient patientDtoToPatient(PatientDto patientDto);

	public PatientDto patientToPatientDto(Patient patient);

	List<Patient> toPatientList(List<PatientDto> patientDto);

	List<PatientDto> toPatientDtoList(List<Patient> patient);
}
