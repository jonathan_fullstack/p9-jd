package mediscreenPatient.mediscreenPatient.controllerTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mediscreenPatient.mediscreenPatient.controller.PatientCrudController;
import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.service.PatientCrudService;

@ExtendWith(MockitoExtension.class)
public class PatientCrudControllerTest {
	private MockMvc mockMvc;

	@InjectMocks
	PatientCrudController patientCrudController;

	@Mock
	PatientCrudService patientCrudService;

	@BeforeEach
	private void setUpPerTest() {
		mockMvc = MockMvcBuilders.standaloneSetup(patientCrudController).build();

	}

	/**
	 * @throws Exception
	 * @Description test get patient wit family
	 */
	@Test
	public void addPatientIdTest() {
		// GIVEN
		LocalDate date = LocalDate.now();
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json",
				java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		// THEN
		try {
			mockMvc.perform(post("/patient/addPatient").accept(MediaType.APPLICATION_JSON)
					.contentType(MEDIA_TYPE_JSON_UTF8).content(objectMapper.writeValueAsString(patientDto)))
					.andDo(print()).andExpect(status().isOk());
			assertEquals(patientCrudService.createPatient(patientDto), ResponseEntity.ok());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @throws Exception
	 * @Description test get patient with family
	 */
	@Test
	public void addPatientIdTesté() {
		// GIVEN
		LocalDate date = LocalDate.now();
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json",
				java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		lenient().when(patientCrudService.createPatient(any())).thenReturn(ResponseEntity.ok("notes is save"));
		// THEN
		ResponseEntity<String> responseEntity = patientCrudController.addPatient(patientDto);

		assertEquals(responseEntity.getStatusCodeValue(), 200);
	}
	
	/**
	 * @throws Exception
	 * @Description test for update  patient with family
	 */
	@Test
	public void updatePatientIdTest() {
		// GIVEN
		LocalDate date = LocalDate.now();
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json",
				java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		lenient().when(patientCrudService.updatePatient(any())).thenReturn(ResponseEntity.ok("notes is save"));
		// THEN
		ResponseEntity<String> responseEntity = patientCrudController.updatePatient(patientDto);

		assertEquals(responseEntity.getStatusCodeValue(), 200);
	}
	
	/**
	 * @throws Exception
	 * @Description test for delete  patient with family
	 */
	@Test
	public void deletePatientIdTest() {
		// GIVEN
		LocalDate date = LocalDate.now();
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json",
				java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		lenient().when(patientCrudService.deletePatient(any())).thenReturn(ResponseEntity.ok("notes is save"));
		// THEN
		ResponseEntity<String> responseEntity = patientCrudController.deletePatient(patientDto);

		assertEquals(responseEntity.getStatusCodeValue(), 200);
	}
}
