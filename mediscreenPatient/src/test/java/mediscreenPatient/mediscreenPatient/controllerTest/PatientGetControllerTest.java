package mediscreenPatient.mediscreenPatient.controllerTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mediscreenPatient.mediscreenPatient.controller.PatientGetController;
import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.service.PatientGetService;

@ExtendWith(MockitoExtension.class)
public class PatientGetControllerTest {
	private MockMvc mockMvc;

	@InjectMocks
	PatientGetController patientGetController;

	@Mock
	PatientGetService patientGetService;

	@BeforeEach
	private void setUpPerTest() {
		mockMvc = MockMvcBuilders.standaloneSetup(patientGetController).build();

	}

	/**
	 * @throws Exception
	 * @Description test create person
	 */
	@Test
	public void getListPatientSucces() throws Exception {
		// GIVEN
		mockMvc.perform(get("/patient/listPatient").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful());

	}

	/**
	 * @throws Exception
	 * @Description test get patient wit family
	 */
	@Test
	public void getPatientWithFamilyTest() {
		// GIVEN
		LocalDate date = LocalDate.now();
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json",
				java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		// THEN
		try {
			mockMvc.perform(post("/patient/getPatientWithFamily").accept(MediaType.APPLICATION_JSON)
					.contentType(MEDIA_TYPE_JSON_UTF8).content(objectMapper.writeValueAsString(patientDto)))
					.andDo(print()).andExpect(status().isOk());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @throws Exception
	 * @Description test get patient wit family
	 */
	@Test
	public void getPatientWithPatientIdTest() {
		// GIVEN
		LocalDate date = LocalDate.now();
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		MediaType MEDIA_TYPE_JSON_UTF8 = new MediaType("application", "json",
				java.nio.charset.Charset.forName("UTF-8"));
		ObjectMapper objectMapper = new ObjectMapper();
		// WHEN
		// THEN
		try {
			mockMvc.perform(post("/patient/getPatientWithPatientId").accept(MediaType.APPLICATION_JSON)
					.contentType(MEDIA_TYPE_JSON_UTF8).content(objectMapper.writeValueAsString(18))).andDo(print())
					.andExpect(status().isOk());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
