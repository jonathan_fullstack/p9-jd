package mediscreenPatient.mediscreenPatient.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.entity.Patient;
import mediscreenPatient.mediscreenPatient.repository.PatientRepository;
import mediscreenPatient.mediscreenPatient.service.PatientCrudService;
import mediscreenPatient.mediscreenPatient.utils.PatientMapper;

@ExtendWith(MockitoExtension.class)
public class PatientCrudServiceTest {

	@Spy
	@InjectMocks
	private PatientCrudService patientCrudService = new PatientCrudService();

	@Autowired
	PatientMapper patientMapper;

	@Mock
	PatientRepository patientRepository;

	@Autowired
	private ModelMapper modelMapper;

	/**
	 * @Description method for test create patient with success
	 */
	@Test
	public void createPatientWithSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		// WHEN
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(patient);
		// THEN
		patientCrudService.createPatient(patientDto);
		verify(patientCrudService).createPatient(patientDto);
	}
	
	/**
	 * @Description method for test create patient with success
	 */
	@Test
	public void createPatientMethod2WithSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		// WHEN
		Optional<Patient> pat = Optional.empty();
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(null);
		// THEN
		patientCrudService.createPatient(patientDto);
		verify(patientCrudService).createPatient(patientDto);
	}

	/**
	 * @Description method for test create patient with error
	 */
	@Test
	public void createPatientWithError() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		patientRepository.save(patient);
		// WHEN
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(patient);
		// THEN
		assertEquals(patientCrudService.createPatient(patientDto),
				ResponseEntity.badRequest().body("Patient exist in db"));
		patientRepository.delete(patient);
	}

	/**
	 * @Description method for test update patient with success
	 */
	@Test
	public void updatePatientWithSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		patientRepository.save(patient);
		// WHEN
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(patient);
		// THEN
		patientCrudService.updatePatient(patientDto);
		verify(patientCrudService).updatePatient(patientDto);
	}

	/**
	 * @Description method for test update patient with error
	 */
	@Test
	public void updatePatientWithError() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		// WHEN
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(null);
		// THEN
		assertEquals(patientCrudService.updatePatient(patientDto),
				ResponseEntity.badRequest().body("Not patient found"));
	}

	/**
	 * @Description method for test delete patient with success
	 */
	@Test
	public void deletePatientWithSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		// WHEN
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(patient);
		// THEN
		patientCrudService.deletePatient(patientDto);
		verify(patientCrudService).deletePatient(patientDto);
	}

	/**
	 * @Description method for convertDate with success
	 */
	@Test
	public void convertDateWithSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		LocalDateTime dateTime = LocalDateTime.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		// WHEN
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(patient);
		// THEN
		patientCrudService.convertToLocalDateTimeViaInstant(date);
		verify(patientCrudService).convertToLocalDateTimeViaInstant(date);
	}
}