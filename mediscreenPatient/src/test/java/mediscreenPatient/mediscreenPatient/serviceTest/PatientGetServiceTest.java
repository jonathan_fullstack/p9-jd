package mediscreenPatient.mediscreenPatient.serviceTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import mediscreenPatient.mediscreenPatient.dto.Notes;
import mediscreenPatient.mediscreenPatient.dto.PatientDto;
import mediscreenPatient.mediscreenPatient.entity.Patient;
import mediscreenPatient.mediscreenPatient.repository.PatientRepository;
import mediscreenPatient.mediscreenPatient.service.NotesFeignService;
import mediscreenPatient.mediscreenPatient.service.PatientGetService;
import mediscreenPatient.mediscreenPatient.utils.PatientMapper;

@ExtendWith(MockitoExtension.class)
public class PatientGetServiceTest {

	@Spy
	@InjectMocks
	private PatientGetService patientGetService = new PatientGetService();

	@Mock
	PatientMapper patientMapper;

	@Mock
	PatientRepository patientRepository;

	@Mock
	private ModelMapper modelMapper;

	@Mock
	NotesFeignService notesFeignServices;

	@BeforeEach
	private void setUpPerTest() {
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");

	}

	/**
	 * @Description method for get patient with success
	 */
	@Test
	public void getPatientWithSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		patientDto.setAddress("4 chemin du test");
		patientDto.setSex("m");
		patientDto.setPhone("122-4555-657");
		patientDto.setFirstName("fghjk");
		patientDto.setDob(date);
		patientDto.setLastName("test family");
		// WHEN
		lenient().when(patientRepository.findByLastName(anyString())).thenReturn(patient);
		lenient().when(patientMapper.patientToPatientDto(any())).thenReturn(patientDto);
		PatientDto patDto = patientMapper.patientToPatientDto(patient);

		// THEN
		patientGetService.getPatient(patientDto);
		verify(patientGetService).getPatient(patientDto);
	}

	/**
	 * @Description method for get patient with success
	 */
	@Test
	public void getPatientByIdWithSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setId(2);
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		patient.setLastName("test family");
		PatientDto patientDto = new PatientDto();
		// WHEN
		lenient().when(patientRepository.findById(any())).thenReturn(Optional.of(patient));
		patientDto = modelMapper.map(patient, PatientDto.class);

		// THEN
		patientGetService.getPatientByPatientId(patient.getId());
		verify(patientGetService).getPatientByPatientId(patient.getId());
	}

	/**
	 * @Description method for get note
	 */
	@Test
	public void returnNotesWithsSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setId(2);
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		Notes notes = new Notes();
		notes.setCategory("None");
		notes.setCreateDate(date);
		notes.setUpdateDate(date);
		notes.setNotes("gygy");

		PatientDto patientDto = new PatientDto();
		// WHEN
		lenient().when(notesFeignServices.getById(anyString())).thenReturn(notes);

		// THEN
		assertEquals(patientGetService.returnNotes((long) 18), notes.getNote());
	}

	/**
	 * @Description method for get category
	 */
	@Test
	public void returnCategoryWithsSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setId(2);
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		Notes notes = new Notes();
		notes.setCategory("None");
		notes.setCreateDate(date);
		notes.setUpdateDate(date);
		notes.setNotes("gygy");

		PatientDto patientDto = new PatientDto();
		// WHEN
		lenient().when(notesFeignServices.getById(anyString())).thenReturn(notes);

		// THEN
		assertEquals(patientGetService.returnCategory((long) 18), notes.getCategory());
	}

	/**
	 * @Description method for get updatedDate
	 */
	@Test
	public void returnUpdateDateWithsSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setId(2);
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		Notes notes = new Notes();
		notes.setCategory("None");
		notes.setCreateDate(date);
		notes.setUpdateDate(date);
		notes.setNotes("gygy");

		PatientDto patientDto = new PatientDto();
		// WHEN
		lenient().when(notesFeignServices.getById(anyString())).thenReturn(notes);

		// THEN
		patientGetService.returnUpdateDate(((long) 18));
		assertEquals(patientGetService.returnUpdateDate((long) 18), notes.getUpdateDate());
	}

	/**
	 * @Description method for get createdDate
	 */
	@Test
	public void returnCreatedDateWithsSuccess() {
		// GIVEN
		Patient patient = new Patient();
		LocalDate date = LocalDate.now();
		patient.setId(2);
		patient.setAddress("4 chemin du test");
		patient.setSex("m");
		patient.setPhone("122-4555-657");
		patient.setFirstName("fghjk");
		patient.setDob(date);
		Notes notes = new Notes();
		notes.setCategory("None");
		notes.setCreateDate(date);
		notes.setUpdateDate(date);
		notes.setNotes("gygy");

		PatientDto patientDto = new PatientDto();
		// WHEN
		lenient().when(notesFeignServices.getById(anyString())).thenReturn(notes);

		// THEN
		patientGetService.returnCreatedDate((long) 18);
		assertEquals(patientGetService.returnCreatedDate((long) 18), notes.getCreateDate());
	}
}
