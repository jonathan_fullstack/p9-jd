package mediscreenAuth;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import mediscreenAuth.config.ExcludeFromJacocoGeneratedReport;

import org.springframework.cloud.openfeign.EnableFeignClients;


@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class AuthApplication {
	@ExcludeFromJacocoGeneratedReport
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
        System.out.println("version 2.1.0");
    }

    @Bean
  //  @LoadBalanced
	@ExcludeFromJacocoGeneratedReport
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
