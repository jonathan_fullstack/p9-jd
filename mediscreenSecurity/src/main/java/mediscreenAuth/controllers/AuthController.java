package mediscreenAuth.controllers;

import mediscreenAuth.dto.LoginDto;
import mediscreenAuth.entities.AuthRequest;
import mediscreenAuth.entities.AuthResponse;
import mediscreenAuth.entities.value_objects.UserVO;
import mediscreenAuth.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(final AuthService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "/register")
    public ResponseEntity<ResponseEntity<UserVO>> register(@RequestBody AuthRequest authRequest) {

        return ResponseEntity.ok(authService.register(authRequest));
    }

    @PostMapping(value = "/login")
    public ResponseEntity<AuthResponse> login(@RequestBody LoginDto login) {
      if (login != null) {
        ResponseEntity.badRequest().body("login is null");
      }
        return ResponseEntity.ok(authService.login(login));
    }


}
