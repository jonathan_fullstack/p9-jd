package mediscreenAuth.dto;

import lombok.Data;

@Data
public class LoginDto {
	String lastName;
	String password;
}
