package mediscreenAuth.entities;

import lombok.Data;

@Data
public class AuthResponse {

	private String accessToken;
	private String refreshToken;
	private int id;
	private String firstName;
	private String lastName;

}
