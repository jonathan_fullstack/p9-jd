package mediscreenAuth.services;

import org.apache.catalina.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import mediscreenAuth.config.ExcludeFromJacocoGeneratedReport;
import mediscreenAuth.dto.LoginDto;
import mediscreenAuth.entities.AuthRequest;
import mediscreenAuth.entities.AuthResponse;
import mediscreenAuth.entities.value_objects.UserVO;
import mediscreenAuth.services.feignClient.MediscreenPatientFeign;

@Service
public class AuthService {

	// private final RestTemplate restTemplate;
	private final JwtUtil jwt;

	@Autowired
	public AuthService(RestTemplate restTemplate, final JwtUtil jwt) {
		// this.restTemplate = restTemplate;
		this.jwt = jwt;
	}

	Logger logger = LoggerFactory.getLogger(AuthService.class);

	@Autowired
	MediscreenPatientFeign mediscreenPatientFeign;
  static final  String userVoCreateUrl = "http://localhost:9002/users" ;
  static final  String userVoLoginUrl = "http://localhost:9002/users/login" ;
  static final  String userVoLoginUrlBis = "http://mediscreen-doctor:9002/users/login" ;

	@ExcludeFromJacocoGeneratedReport
	public ResponseEntity<UserVO> register(AuthRequest authRequest) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
		// do validation if user already exists
		authRequest.setPassword(BCrypt.hashpw(authRequest.getPassword(), BCrypt.gensalt()));
    HttpEntity<AuthRequest> httpEntity = new HttpEntity<AuthRequest>(authRequest, headers);
    RestTemplate restTemplate = new RestTemplate();
    UserVO userVO = restTemplate.postForObject(userVoCreateUrl, httpEntity, UserVO.class);
		return ResponseEntity.ok(userVO);

	}

	@ExcludeFromJacocoGeneratedReport
	public AuthResponse login(LoginDto login) {
    UserVO userVO = new UserVO();
    if (login.getLastName() != null && login.getPassword() != null) {
       userVO = mediscreenPatientFeign.login(login);
    } else {
      AuthResponse ar = new AuthResponse();
      ar.setLastName("null");
      ar.setAccessToken("0");
     return ar;
    }

//    	 UserVO userVO = restTemplate.postForObject(url, login, UserVO.class);

  // if (BCrypt.checkpw(login.getPassword(), userVO.getPassword())) {
      if (userVO.getLastName() != null) {
        String accessToken = jwt.generate(userVO, "ACCESS");
        String refreshToken = jwt.generate(userVO, "REFRESH");
        AuthResponse ar = new AuthResponse();
        ar.setAccessToken(accessToken);
        ar.setRefreshToken(refreshToken);
        ar.setId( userVO.getId());
        ar.setFirstName(userVO.getFirstName());
        ar.setLastName(userVO.getLastName());
        return ar;
      } else {
        throw new RuntimeException("Not user found");
      }
  //  } else {
   //   throw new RuntimeException("Error with this password");
  //  }

  }

}
