package mediscreenAuth.services.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import mediscreenAuth.dto.LoginDto;
import mediscreenAuth.entities.AuthRequest;
import mediscreenAuth.entities.value_objects.UserVO;

@FeignClient(name = "mediscreen-doctor",url = "host.docker.internal:9002" )
public interface MediscreenPatientFeign {

	@PostMapping("/users")
	public UserVO save(@RequestBody AuthRequest user);

	@PostMapping("/users/login")
	public UserVO login(@RequestBody LoginDto login);
  @Component
  class HelloServiceFallback implements MediscreenPatientFeign {

    public String hello() {
      return null;
    }

    public void exception() { }

    public String none() {
      return "Fallback!";
    }

    @Override
    public UserVO save(AuthRequest user) {
      return null;
    }

    @Override
    public UserVO login(LoginDto login) {
      return null;
    }
  }
}
