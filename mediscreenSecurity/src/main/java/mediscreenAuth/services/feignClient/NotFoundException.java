package mediscreenAuth.services.feignClient;

import org.springframework.http.ResponseEntity;

public class NotFoundException extends Exception {

  public NotFoundException (String s) {
    super(s) ;
    ResponseEntity.badRequest().body("url not found");
    System.out.println("url not found");

  }
}
