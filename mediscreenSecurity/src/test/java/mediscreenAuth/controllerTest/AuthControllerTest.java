package mediscreenAuth.controllerTest;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import mediscreenAuth.controllers.AuthController;
import mediscreenAuth.dto.LoginDto;
import mediscreenAuth.entities.AuthRequest;
import mediscreenAuth.entities.AuthResponse;
import mediscreenAuth.entities.value_objects.UserVO;
import mediscreenAuth.services.AuthService;


@ExtendWith(MockitoExtension.class)
public class AuthControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	AuthController authController;

	@Mock
	AuthService authService;

	@BeforeEach
	private void setUpPerTest() {
		mockMvc = MockMvcBuilders.standaloneSetup(authController).build();

	}

	/**
	 * @throws Exception
	 * @Description test method controller  for register
	 */
	@Test
	public void registerControllerTest2() {
		//GIVEN
		LocalDate date = LocalDate.now();
		AuthRequest authRequest = new AuthRequest();
		authRequest.setEmail("hhsssssh@ggg.com");
		authRequest.setFirstName("jojo");
		authRequest.setLastName("hggfty");
		 authRequest.setPassword(BCrypt.hashpw("!!Djulia77777@", BCrypt.gensalt()));
		UserVO userVO = new UserVO();
		userVO.setEmail("hgvsssgv@gg.com");
		userVO.setFirstName("yttf");
		userVO.setLastName("hft");
		userVO.setPassword("gfht");
		userVO.setRole("admin");
		//WHEN
		lenient().when(authService.register(any())).thenReturn(ResponseEntity.ok(userVO));
		ResponseEntity<ResponseEntity<UserVO>> responseEntity = authController.register(authRequest);

		assertEquals(responseEntity.getStatusCodeValue(), 200);
	//	assertEquals(responseEntity.getHeaders().getLocation().getPath()).isEqualTo("/1");
	}

	/**
	 * @throws Exception
	 * @Description test method controller  for login
	 */
	@Test
	public void loginControllerTest2() {
		//GIVEN
		LocalDate date = LocalDate.now();
		AuthRequest authRequest = new AuthRequest();
		authRequest.setEmail("hhsssssh@ggg.com");
		authRequest.setFirstName("jojo");
		authRequest.setLastName("hggfty");
		 authRequest.setPassword(BCrypt.hashpw("!!Djulia77777@", BCrypt.gensalt()));
		UserVO userVO = new UserVO();
		userVO.setEmail("hgvsssgv@gg.com");
		userVO.setFirstName("yttf");
		userVO.setLastName("hft");
		userVO.setPassword("gfht");
		userVO.setRole("admin");
		AuthResponse authResponse = new AuthResponse();
		authResponse.setAccessToken("jhgg");
		authResponse.setFirstName("ssss");
		authResponse.setId(5);
		authResponse.setRefreshToken("hggf");
		authResponse.setLastName("jojo");
		LoginDto login = new LoginDto();
		login.setLastName("jojo");
		login.setPassword("@@Djulia77130");
		//WHEN
		lenient().when(authService.login(any())).thenReturn(authResponse);
		ResponseEntity<AuthResponse> responseEntity = authController.login(login);

		assertEquals(responseEntity.getStatusCodeValue(), 200);
	//	assertEquals(responseEntity.getHeaders().getLocation().getPath()).isEqualTo("/1");
	}

}
