package mediscreenAuth.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;

import mediscreenAuth.dto.LoginDto;
import mediscreenAuth.entities.AuthRequest;
import mediscreenAuth.entities.value_objects.UserVO;
import mediscreenAuth.services.AuthService;
import mediscreenAuth.services.feignClient.MediscreenPatientFeign;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {

	@Spy
	@InjectMocks
	AuthService authService;

	@Mock
	MediscreenPatientFeign mediscreenPatientFeing;

	/**
	 * @Description method for register with success
	 */
	@Test
	public void registerWithSuccess() {
		// GIVEN
		AuthRequest authRequest = new AuthRequest();
		authRequest.setEmail("hhsssssh@ggg.com");
		authRequest.setFirstName("jojo");
		authRequest.setLastName("hggfty");
		authRequest.setPassword(BCrypt.hashpw("!!Djulia77777@", BCrypt.gensalt()));
		UserVO userVO = new UserVO();
		userVO.setEmail("hgvsssgv@gg.com");
		userVO.setFirstName("yttf");
		userVO.setLastName("hft");
		userVO.setPassword("gfht");
		userVO.setRole("admin");
		// WHEN
		// lenient().when(mediscreenPatientFeing.save(authRequest)).thenReturn(userVO);
		Mockito.when(mediscreenPatientFeing.save(any(AuthRequest.class))).thenReturn(userVO);
		assertNotNull(mediscreenPatientFeing.save(authRequest));
		// THEN

	}

	
}
